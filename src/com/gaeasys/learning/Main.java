package com.gaeasys.learning;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {

        try(Connection conn = DriverManager.getConnection(
                "jdbc:mysql://127.0.0.1:3306/java_database", "root","") ){
            if(conn != null){
                System.out.println("Connected to the database!");
            }else{
                System.out.println("fail");
            }
            ResultSet prodResultSet = conn.prepareStatement("SELECT product_id, product_name, product_qty, product_price from products").executeQuery();
            ResultSet invResultSet = conn.prepareStatement("SELECT Invoice_ID, Date from invoices").executeQuery();
            ResultSet invLineItemResultSet = conn.prepareStatement("SELECT Invoice_Line_Item_ID, Product_ID,Prodcut_Line_Item_QTY,Invoice_ID FROM invoice_line_item").executeQuery();

            // code below: add invoice line item array to map
            Map<Integer, ArrayList<InvoiceLineItem> > invLineItemArrMap = new HashMap<>();

            while (invLineItemResultSet.next() ){

                InvoiceLineItem invLineItem = new InvoiceLineItem();
                invLineItem.IltId = invLineItemResultSet.getInt("Invoice_Line_Item_ID");
                invLineItem.ProId =invLineItemResultSet.getInt("Product_ID");
                invLineItem.QtyId =invLineItemResultSet.getInt("Prodcut_Line_Item_QTY");
                invLineItem.invoiceId =invLineItemResultSet.getInt("Invoice_ID");



                if(invLineItemArrMap.containsKey(invLineItem.invoiceId) == true){

                    invLineItemArrMap.get(invLineItem.invoiceId).add(invLineItem);

                }else{

                    ArrayList<InvoiceLineItem> InvLineItemArray = new ArrayList<>();
                    InvLineItemArray.add(invLineItem);

                    invLineItemArrMap.put(invLineItem.invoiceId, InvLineItemArray);

                }

            }

            // code below add invoice to array

            ArrayList<Invoice> InvArr =new ArrayList<>();

            while (invResultSet.next() ){
                Invoice Invo = new Invoice();
                Invo.invoice_id = invResultSet.getInt("Invoice_Id");
                Invo.invoice_date = invResultSet.getDate("Date");
                InvArr.add(Invo);

                System.out.println();
            }


            // code below add product to map

            ArrayList<Integer> arr = new ArrayList<>();


            Map<Integer,Product> prodMap=new HashMap<>();

            while (prodResultSet.next()){

                Product prod = new Product();
                prod.Product_Id = prodResultSet.getInt("Product_Id");
                prod.Product_Name = prodResultSet.getString("Product_Name");
                prod.Product_Qty = prodResultSet.getInt("Product_Qty");
                prod.Product_Price = prodResultSet.getInt("Product_Price");



                prodMap.put( prod.Product_Id , prod);


                arr.add(prodResultSet.getInt("product_price"));
            }



            //

            for(Invoice eachInv : InvArr){

                System.out.println("Invoice id : "+eachInv.invoice_id);

                System.out.println("invoice date : "+eachInv.invoice_date);

                ArrayList<InvoiceLineItem> InvLineItemArray = invLineItemArrMap.get(eachInv.invoice_id);


                for(InvoiceLineItem eachLine : InvLineItemArray){

                    System.out.println("\t - prod name : " + prodMap.get(eachLine.ProId).Product_Name);
                    System.out.println("\t   prod price : " + prodMap.get(eachLine.ProId).Product_Price);
                }


                System.out.println("-------------------");

            }
            System.out.println("Total price max's : "+findMax(arr));

        }catch (SQLException e  ){
            e.printStackTrace();
        }
    }
    //for get function
    public static int findMax(ArrayList<Integer> a) {

        int max = a.get(0);

        for (int i = 1; i < a.size(); i++) {
            if (a.get(i) > max) {
                max = a.get(i);
            }
        }

        return max;
    }
}
